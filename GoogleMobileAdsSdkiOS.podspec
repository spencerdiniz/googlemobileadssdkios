Pod::Spec.new do |s|

  s.name         = "GoogleMobileAdsSdkiOS"
  s.version      = "6.11.1"
  s.summary      = "Google AdMob Ads SDK."

  s.description  = <<-DESC
                   The Google AdMob Ads SDK allows developers to easily incorporate mobile-friendly text and image banners as well as rich, full-screen web apps known as interstitials.
                   DESC

  s.homepage     = "https://developers.google.com/mobile-ads-sdk/docs/"
  s.license      = { :type => 'Google Inc.', :text => <<-LICENSE
                                                      Copyright 2009 - 2012 Google, Inc. All rights reserved.
                                                      LICENSE
                   }
  s.author       = "Google Inc."
  s.platform     = :ios
  s.source       = { :http => "https://bitbucket.org/spencerdiniz/googlemobileadssdkios/raw/master/googlemobileadssdkios.zip" }
  s.source_files = "GoogleMobileAdsSdkiOS-6.11.1/**/*.{h}"
  s.preserve_paths = "GoogleMobileAdsSdkiOS-6.11.1"

  s.frameworks = "AVFoundation", "AudioToolbox", "CoreTelephony", "MessageUI", "SystemConfiguration", "CoreGraphics", "AdSupport", "StoreKit"
  s.library   = "GoogleAdMobAds"

  s.requires_arc = false
  s.xcconfig = { "LIBRARY_SEARCH_PATHS" => "$(PODS_ROOT)/GoogleMobileAdsSdkiOS/GoogleMobileAdsSdkiOS-6.11.1" }
end
